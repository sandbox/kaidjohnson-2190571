(function($) {

Drupal.behaviors.scrollDepth = {
  attach: function(context, settings) {
    if ($.scrollDepth) {
      $.scrollDepth();
    }
  }
}

})(jQuery);
